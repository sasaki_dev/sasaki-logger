# sasaki-logger

Standard logging library for Sasaki apps - intended to be adaptable should we change logging providers.

## Installation

```bash
npm install git+https://bitbucket.org/sasaki_dev/sasaki-logger.git --save
```

## Usage

This library borrows from (and is intended to be superficially compatable with) [Winston](https://github.com/flatiron/winston).

```js
var logger = new (require('sasaki-logger').Logger)();
```

## Environment Variables

```bash

```