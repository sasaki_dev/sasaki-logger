var nodeEnv = process.env.NODE_ENV || 'production';
var isLocal = function () {
    return nodeEnv === 'localhost';
};
var logToConsole = process.env.LOG_TYPE === 'console' || (isLocal() && process.env.LOG_TYPE == null);

module.exports = {
    logToConsole: logToConsole
};