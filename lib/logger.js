//region node.js core
var util = require('util');
//endregion
//region npm modules

//endregion
//region modules
var config = require('./config');
//endregion

var Level = {//based on logentries severity codes
    'DEBUG': 'debug',
    'INFO': 'info',
    'NOTICE': 'notice',
    'WARNING': 'warning',
    'ERROR': 'err',
    'CRIT': 'crit',
    'ALERT': 'alert',
    'EMERGENCY': 'emerg'
};

var Action = {
    'DO_NOTHING': 'do nothing',//--ignore this level e.g. when not debugging
    'CONSOLE_ONLY': 'console',
    'CENTRAL': 'central',//--use a centralized logging service as defined in config
    'CONSOLE_AND_CENTRAL': 'console_and_central'
};

/**
 @class Logger
 */
Logger = function () {
    var _self = this;

    //region private fields and methods
    var _actionsByLevel = {};

    var _init = function () {
        //setup default logging levels, can be overridden using setAction
        _actionsByLevel[Level.DEBUG] = Action.DO_NOTHING;
        _setDefaultAction(Action.CONSOLE_AND_CENTRAL);
    };

    var _setDefaultAction = function(action) {
        _actionsByLevel[Level.INFO] = action;
        _actionsByLevel[Level.NOTICE] = action;
        _actionsByLevel[Level.WARNING] = action;
        _actionsByLevel[Level.ERROR] = action;
        _actionsByLevel[Level.CRIT] = action;
        _actionsByLevel[Level.ALERT] = action;
        _actionsByLevel[Level.EMERGENCY] = action;
    };

    var _stringify = function (level, msg, meta) {
        return level + ':' + util.format(msg, meta);
    };

    var _log = function (level, msg, meta, callback) {
        if (config.logToConsole) {
            console.log(_stringify(level, msg, meta));
        } else {

        }
    };
    //endregion

    //region public API


    /**
     * @param level {string}
     * @param msg {string}
     * @param [meta] {Object}
     * @param [callback] {function}
     */
    this.log = function (level) {
        //copied from Winston https://github.com/flatiron/winston/blob/master/lib/winston/logger.js#L123
        var args = Array.prototype.slice.call(arguments, 1);

        while (args[args.length - 1] === null) {
            args.pop();
        }

        var callback = typeof args[args.length - 1] === 'function' ? args.pop() : null;
        var meta = typeof args[args.length - 1] === 'object' ? args.pop() : {};
        var msg = util.format.apply(null, args);

        _log(level, msg, meta, callback);
    };


    this.metric = function () {

    };


    this.setAction = function (level, action) {
        _actionsByLevel[level] = action;
    };

    this.setDefaultAction = function (level, action) {
        _setDefaultAction(action);
    };
    //endregion

    _init();
};

module.exports.Logger = Logger;
module.exports.Level = Level;
module.exports.Action = Action;

